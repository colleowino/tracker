import Vue from "vue";
import App from "./App.vue";
import VueLayers from "vuelayers";
import "vuelayers/lib/style.css";

import Buefy from "buefy";
import "buefy/dist/buefy.css";

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

Vue.use(VueLayers);
Vue.use(Buefy);
Vue.use(Toast);

Vue.config.productionTip = false;
new Vue({
  render: (h) => h(App),
}).$mount("#app");
